<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppointmentController extends AbstractController
{
    /**
     * @Route("/appointments", name="appointment_list")
     */
    public function list(Request $request): Response
    {
        // Retrieve appointments here
    }

    /**
     * @Route("/appointments/create", name="appointment_create")
     */
    public function create(Request $request): Response
    {
        // Handle appointment creation here
    }

    /**
     * @Route("/appointments/{id}/edit", name="appointment_edit")
     */
    public function edit(Request $request, $id): Response
    {
        // Handle appointment editing here
    }

    /**
     * @Route("/appointments/{id}/delete", name="appointment_delete")
     */
    public function delete(Request $request, $id): Response
    {
        // Handle appointment deletion here
    }
}
