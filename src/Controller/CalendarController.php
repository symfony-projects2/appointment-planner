<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendarController extends AbstractController
{
    /**
     * @Route("/calendar/{year}/{month}", name="calendar_view", requirements={"year"="\d{4}","month"="\d{2}"})
     */
    public function view(Request $request, $year, $month): Response
    {
        // Display the calendar view for the specified month and year
    }

    /**
     * @Route("/calendar/{year}/{month}/{week}", name="calendar_week_view", requirements={"year"="\d{4}","month"="\d{2}","week"="\d{2}"})
     */
    public function weekView(Request $request, $year, $month, $week): Response
    {
        // Display the calendar view for the specified week, month, and year
    }

    /**
     * @Route("/calendar/{year}/{month}/next", name="calendar_next_month", requirements={"year"="\d{4}","month"="\d{2}"})
     */
    public function nextMonth(Request $request, $year, $month): Response
    {
        // Display the calendar view for the next month
    }

    /**
     * @Route("/calendar/{year}/{month}/previous", name="calendar_previous_month", requirements={"year"="\d{4}","month"="\d{2}"})
     */
    public function previousMonth(Request $request, $year, $month): Response
    {
        // Display the calendar view for the previous month
    }

    /**
     * @Route("/calendar/{year}/{month}/{week}/next", name="calendar_next_week", requirements={"year"="\d{4}","month"="\d{2}","week"="\d{2}"})
     */
    public function nextWeek(Request $request, $year, $month, $week): Response
    {
        // Display the calendar view for the next week
    }

    /**
     * @Route("/calendar/{year}/{month}/{week}/previous", name="calendar_previous_week", requirements={"year"="\d{4}","month"="\d{2}","week"="\d{2}"})
     */
    public function previousWeek(Request $request, $year, $month, $week): Response
    {
        // Display the calendar view for the previous week
    }
}
