
import React from "react";

import Navbar from "./components/Navbar";
import { BrowserRouter, Switch, Route } from "react-router-dom";

// PAGES
import Hompage from "./pages/Hompage";
import About from "./pages/About";
import Calendar from "./components/Calendar";
import store from './redux/store';
import {Provider} from 'react-redux';
import {RootContainer} from './styles/AppStyles';

export default function Home() {
  return (
    <div className="App">
       <RootContainer>
          <BrowserRouter>
            <Provider store = {store}>
              <Navbar />
                <Switch>
                  <Route path="/products" component={About} />
                  <Route exact path="/">
                    <Calendar/>
                  </Route>
                  <Route path="/year/:year/month/:monthDate">
                      <Calendar/>
                  </Route>
                </Switch>
            </Provider>
          </BrowserRouter>
       </RootContainer>
    </div>
  );
}

